\renewcommand{\thechapter}{\Alph{chapter}}

# Résultats de la comparaison des services d'hébergement en nuage

\includegraphics[angle=90,height=0.967\textheight]{HostingProviderComp.pdf}

# Résultats de la comparaison des solutions de déploiement

\includegraphics[height=0.967\textheight]{ServerAdminToolsComp.pdf}
