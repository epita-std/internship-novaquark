%%define putpic($x , $y , $width , $path)
    return "\\begin{tikzpicture}[remember picture,overlay]" .
	       "\\node at ($x,$y) {" .
	       "\\includegraphics[width=$width]{$path}};" .
		   "\\end{tikzpicture}"
=end

% Soutenance de stage de fin d'études
% Pierre-Olivier Mercier
% Jeudi 4 septembre 2014

# Novaquark

## L'entreprise

* Startup créée en janvier 2014 ;
* fondée par Jean-Christophe Baillie ;
* de 4 collaborateurs à 14 aujourd'hui ;
* actuellement dans l'incubateur Agoranov.

%%image(../templates/nemu/images/IMG_20140812.jpg, 0.9)

## \textsc{Dual}

\vspace*{-0.8cm}

%%image(../templates/nemu/images/game.png, 0.7)

\vspace*{-0.9cm}

* Jeu-vidéo MMO ;
* monde unique, éditable, rendu par *Voxels* ;
* économie, gameplay émergent : où les joueurs construisent leur propre jeu via leurs interactions.


# Le stage

## Présentation du sujet

* Travail avec l'équipe en charge du serveur ;
* mise en place des bases de l'infrastructure de mise à l'échelle :
	* virtualisation, *cloud computing* ;
    * relevé de métriques,
	* déploiement centralisé,
	* sécurité du cluster ;
* tests de montée en charge du serveur :
    * déploiement automatique,
	* dans les nuages.


# Travail effectué

## Relevé de métriques

%%image(../report/img/grafana-monitor0.png)

## Relevé de métriques

%%image(../report/img/cubism.png)

## Déploiement centralisé


### Recettes Ansible

* Serveurs de conteneurs applicatifs (LXC, Docker) ;
* machines pour les tests de montée en charge ;
* machines virtuelles pour les développeurs.

%%putpic(98mm, 10mm, 2cm, ../templates/nemu/images/ansible)

### Inventaire automatique

Qualification des machines à partir d'une nomenclarure prédéfinie.


## Interface de gestion

%%image(../report/img/ncurses-dev.png)

## Virtualisation légère

### Conteneurs applicatifs

* Site web du jeu ;
* forum ;
* supervision ;
* serveur LDAP.

%%putpic(95mm, 13mm, 4cm, ../templates/nemu/images/docker)


## Tests de montée en charge

### Sur un serveur OVH

* Déploiement d'un serveur de jeu ;
* relevé de métriques système ;
* profiling du code en charge.

### Sur Amazon Web Services EC2

* Construction du disque virtuel par recette Ansible ;
* lancement progressif des machines virtuelles via l'API.


# Conclusion

## Conclusion

%%image(../templates/nemu/images/nqSRS.png, 0.7)
