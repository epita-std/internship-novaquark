## Découpage du stage

### État de l'art

Afin de mesurer l'ampleur de l'architecture qu'il est nécessaire de mettre en
place, mon stage a débuté par plusieurs comparaisons d'outils et
l'apprentissage de certains concepts propres aux jeux vidéo et tout
particulièrement aux jeux en ligne massivement multijoueurs.

\vspace{1em}

Cette étape s'est terminée par le partage avec mon maître de stage de mes
résultats ainsi que leur critique.

### Montée en compétences

Une fois les outils sélectionnés suivants les critères établis au travers de
l'étape précédente, il m'a fallu monter en compétence sur ceux-ci.

### Développements et tests de montée en charge

À la suite de cet apprentissage, j'ai effectué des tests afin de valider les
choix qui avaient été faits, afin d'en tirer les conclusions.


## Respect des délais et critique de ce découpage

À de nombreuses reprises, le planning initialement prévu n'a pu être
respecté afin de faire face aux besoins qui se sont matérialisés à divers
moments. Ainsi, les parties concernant l'intégration de données métriques
dans le serveur, utile dans le long terme, a été repoussé afin de se concentrer
sur des besoins spécifiques de l'équipe comme la mise à disposition d'un
serveur de jeu aux développeurs du client.

\vspace{1em}

Je n'ai pas de critique à formuler quant à ce découpage, somme toute très
classique. Avec les objectifs globaux fixés par l'équipe, j'ai pu m'organiser
de telle sorte que chaque étape coïncide avec les besoins de chacun.


## Nature et fréquence des points de contrôle en interne

Le développement du jeu suit une méthodologie agile : cela a commencé par
Kaban, pour arriver aujourd'hui à Scrum.

\vspace{1em}

Deux fois par semaine, toute l'équipe fait le point sur l'avancée de
chacun. C'est l'occasion de recentrer les priorités.

Tous les jours, nous sommes tenus d'envoyer un petit compte-rendu de nos
activités de la journée, en n'oubliant pas d'indiquer les difficultés que l'on
peut rencontrer. En fonction de la difficulté, un membre de l'équipe peut être
amené à donner son avis sur le problème s'il l'a déjà rencontré par le passé.

\vspace{1em}

Le soir est toujours une bonne occasion de discuter avec mon maître de stage :
que ce soit pour se tenir mutuellement informés de nos activités ou pour faire
le point sur les développements à venir.
