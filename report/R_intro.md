## Présentation de l'entreprise

Novaquark est un studio de jeux vidéo créé en janvier 2014 par Jean-Christophe
\textsc{Baillie}. Il avait précédemment montée la start-up de robotique Gostai,
rachetée par Aldebaran en 2012.

L'équipe se concentre sur la conception d'un jeu vidéo en ligne massivement
multijoueurs dans un monde unique partagé par tous les joueurs : \Dual.

%%fimage(img/nq.png, 0.15, "Le logo sigle de l'entreprise")

### Contexte concurentiel

\Dual a pour ambition d'avoir la dimension spatiale et communautaire du célébre
jeu Eve Online (plus de 500 000 joueurs), tout en permettant aux joueurs
d'évoluer dans un monde éditable (à la manière de Minecraft).

Depuis la création de l'entreprise, de nombreux concurrents sont apparus,
partageant des idées innovantes de \Dual :

- Star Citizen : MMO par le concepteur de jeux vidéo Chris Roberts ;
- No Man's Sky : MMO dans un univers généré de manière procédurale ;
- Untold Universe : projet similaire incubé chez Startup 42 ;
- Blockscape ;
- Space Engineers ;
- Planets Cube ;
- ...

\vspace{1em}

%%fimage(img/dual-dark.png, 0.4, "Le logo du jeu")

### Organisation de l'équipe

Le studio se trouve actuellement au sein de l'incubateur de start-up Agoranov.
À mon arrivée, l'équipe était composée de 4 personnes :

- Jean-Christophe \textsc{Baillie} : fondateur et président de Novaquark ;
- Étienne \textsc{Robin-Champigneul} : COO ;
- Jérome \textsc{Jouvie} : développeur 3D ;
- David \textsc{Bernard} : développeur serveur.

\vspace{1em}

Proche de M. \textsc{Bernard}, j'ai fait mes débuts en tant que DevOps afin de
mener à bien mon sujet de stage.

\vspace{1em}

Depuis, l'équipe s'est aggrandie et se compose aujourd'hui de 10 personnes :
dont un concepteur de jeux-vidéo, un gestionnaire de communauté, un graphiste,
un développeur client, et trois autres stagiaires. Voir l'organigramme figure
\ref{fig:img/organigrame.pdf} en page \pageref{fig:img/organigrame.pdf}.

%%fimage(img/organigrame.pdf, 1, "Organigramme de l'équipe actuelle")

## Sujet et finalités

L'intitulé de mon stage tel qu'il a été validé est le suivant :

> Vous participerez à l’élaboration du cluster en mettant en place des
> outils centralisés de déploiement de machines virtuelles ainsi que
> des tests automatiques de montée en charge et participerez à la mise
> en place des outils permettant de monitorer le réseau en temps
> réel. Vous aurez à définir et justifier les choix d'architecture et
> de technologie en accord avec votre encadrant. L'accent sera en
> particulier mis sur la sécurité pour toutes les parties front-end du
> cluster.

Il s'agit de travailler au sein de l'équipe en charge du développement du
serveur. Mon rôle a été dans un premier temps d'analyser les différentes
solutions existantes de déploiement automatique, puis d'écrire un certain
nombre de *recettes*. Les premières *recettes* concernaient le déploiement des
machines de monitoring.

\vspace{1em}

Par la suite, lorsque le serveur a été suffisamment fonctionnel, j'ai eu à
concevoir le système de test de montée en charge.

\vspace{1em}

De nombreuses tâches annexes m'ont également été attribuées tout au long du
stage en particulier pour connaître mon avis d'administrateur système quant à
l'implémentation de certaines fonctionnalités pouvant influer sur le
déploiement, la charge ou la sécurité.

## Maturité de l'entreprise

L'entreprise démarrant son activité, elle n'avait encore aucune base de travail
liée à mon sujet de stage.

Les autres employés de l'entreprise travaillaient sur d'autres problématiques :
principalement le code du client : moteur de rendu, expérience de jeu, etc.

Mon maître de stage s'est lui occupé du serveur et des problèmatiques réseau
ainsi que d'une partie de l'administration système.

## État de mes connaissances

Fort de mon expérience d'administration système au laboratoire des assistants,
du laboratoire SRS et de nombreuses expériences personnelles, je partais à
l'aise avec les technologies de virtualisation, à la base de l'informatique
en nuage.

\vspace{1em}

Mon travail sur l'environnement du serveur de jeu et de son cœur touche à
l'ensemble des branches de la majeure SRS :

- **système :** avec la recherche d'une architecture permettant
  d'assurer la montée en charge du jeu au fil d'une journée et de la vie
  du jeu ;
- **réseau :** puisqu'il fallait prendre en compte les problématiques
  d'échanges entre les clients et les serveurs, mais aussi entre les
  serveurs eux-mêmes ;
- **sécurité :** car les serveurs seront exposés à
  un grand nombre de personnes qui ne se conteront pas de jouer via le
  client.

## Intérêt du stage pour l'entreprise

Pour l'entreprise, mon stage a permis d'établir :

* les bases pour permettre aux développeurs de travailler avec un serveur de
  jeu,
* la recherche de méthodes pour assurer la mise à l'échelle du jeu une fois
  qu'il sera sorti,
* la participation aux réflexions de design du serveur de jeu pour permettre
  une répartition de charge simple, fiable et aisée.

## Contexte de travail

L'entreprise est établie dans l'incubateur Agoranov ; une pièce nous y a été
attribuée. Nous nous y retrouvons tous pour travailler, il est donc facile de
parler à n'importe qui puisque l'on se trouve dans le même espace. Voir la
figure \ref{fig:img/tinyplanet.jpg} sur la page suivante.

\vspace{1em}

Dès le premier jour, une machine dotée de composants de pointe m'a été
attribuée ; il m'a été laissé le choix du système d'exploitation. Au milieu de
mon stage, j'ai eu besoin de travailler avec un disque dur plus réactif ; à la
suite de ma demande, celui-ci a été commandé dans la semaine.

\vspace{1em}

Arrivé quelques semaines après la création de l'entreprise, j'ai participé à
l'élaboration des premières documentations.

%%fimage(img/tinyplanet.jpg, 0.9, "Notre espace de travail")
