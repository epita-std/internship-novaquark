Chat

:   Moyen de communication instantané entre les joueurs.

\vspace{0.5em}

Cluster

:   Un groupe de machines.

\vspace{0.5em}

Cloud computing

:   Voir *Informatique en nuage*.

\vspace{0.5em}

Conteneur

:   Système d'exploitation complet (programmes, bibliothèques, ...) exécuté par
    un noyau compatible (GNU/Linux, BSD, Solaris). L'idée est de cloisonner,
	voire de restreindre, les actions effectuées par ce conteneur.

\vspace{0.5em}

Déploiement

:   Configurer une machine ou un programme pour qu'il s'exécute d'une manière
    définie.

\vspace{0.5em}

DevOps

:   Personne ou groupe de personnes chargé d'écouter les demandes des
    équipes de développement afin de répondre aux mieux à leur besoin du
    point de vue du système d'information mis à leur disposition.

\vspace{0.5em}

Front-end

:   Partie d'un système exposée à l'utilisateur final. En opposition au
    *back-end* qui lui n'est pas voué à être accédé par l'utilisateur final.

\vspace{0.5em}

Informatique en nuage

:   Abstraction des couches basses d'un système qui ne relèvent pas du
    cœur de métier d'une entreprise. Les premières couches concernées
    par les délégations/sous-traitements sont le matériel, puis le
    système d'exploitation, et peuvent aller jusqu'à la mise à
    disposition d'un logiciel particulier configuré pour un usage
    déterminé.

\vspace{0.5em}

Journaux

:   Liste d'événements survenus au sein d'un programme. En général on
    distingue plusieurs niveaux d'enregistrement en fonction de
    l'utilisation faite du programme : par exemple, en production, on
    ne va enregistrer que les erreurs, tandis que dans un
    environnement de développement, on va enregistrer les
    avertissements ou des informations de développement.

\vspace{0.5em}

Load-balancer

:   Machine ou groupe de machines dont le rôle est d'orienter le trafic
    entrant vers un second groupe de machines, effectuant un travail plus
    lourd.

\vspace{0.5em}

Machine virtuelle

:   Simulation complète d'une machine physique effectuée par un programme de
    virtualisation (hyperviseur ou émulateur) dans le but de tester un système
    ou pour isoler l'exécution de programmes.

\vspace{0.5em}

Monitoring

:   Supervision d'un parc de machines (physiques ou virtuelles), mesures et
    relevé de métriques permettant de s'assurer de la bonne marche des
    installations.

\vspace{0.5em}

Pare-feu

:   Programme filtrant les données passant sur le réseau d'une machine
    connectée afin de supprimer le trafic qui n'est pas jugé légitime
    suivant des critères prédéfinis.

\vspace{0.5em}

Plugin

:   Extension d'un programme se présentant généralement sous la forme
    d'un module, permettant de lui ajouter des fonctionnalités.

\vspace{0.5em}

SSH

:   Protocole sécurisé de connexion à distance dans les environnements
    Unix modernes.
