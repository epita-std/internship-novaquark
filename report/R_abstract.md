Novaquark est un studio de jeux vidéo réalisant un jeu en ligne massivement multijoueur.

\vspace{1em}

Durant les six mois de mon stage, j'ai travaillé au sein de l'équipe en charge
du développement du serveur, dans le but de mettre en place les bases de
l'infrastructure d'informatique en nuage nécessaires à la mise à l'échelle
automatiquement du nombre de serveurs en fonction du nombre de joueurs
connectés.

\vspace{1em}

Le jeu étant encore en phase de développement intensif, j'ai donc surtout
analysé les différentes solutions d'hébergement, de déploiement, etc. avant de
monter en compétence sur les technologies retenues.

\vspace{1em}

En tant que *DevOps*, j'ai été amené à concevoir un système répondant aux
besoins émis par les développeurs d'avoir un serveur de jeu dans des versions
différentes en fonction de leur rôle au sein de l'équipe.

J'ai aussi effectué les tests de montée en charge à la demande de l'équipe en
charge du serveur et ai participé aux réflexions de son développement afin
d'intégrer au plus tôt les remarques concernant la mise à l'échelle et la
sécurité.
