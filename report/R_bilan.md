## Intérêt du stage pour l'entreprise

À l'issue de mon stage, Novaquark a maintenant des bases solides pour
tout ce qui gravite autour du serveur de jeu :

- les développeurs peuvent disposer, en fonction de leur besoins,
  d'une machine virtuelle avec des outils de développement adaptés au
  serveur, ou d'une machine virtuelle clef en main avec une interface
  simplifiée, mais aussi d'une machine globale où aucune configuration
  n'est nécessaire (lorsqu'ils n'ont pas de tests particuliers à
  faire) ;
- chaque conteneur, machine virtuelle ou serveur configuré dispose
  d'une recette de déploiement qui permet de le configurer, reconfigurer
  ou de le mettre à jour ;
- les métriques relevées sont d'ores et déjà affichables dans des
  tableaux de bords pour corréler les informations en provenance des
  différents capteurs.

\vspace{1em}

D'autre part, j'ai également permis d'éclaircir certains points encore
obscurs tels que l'authentification, le système de chat ou encore
l'architecture de déploiement et de mise à jour. Cela sera mis en
place lorsque les développeurs seront plus avancés sur ces points.


## Intérêt personnel

Ce stage m'a permis de découvrir et d'approfondir de nombreux aspects
tels que le déploiement automatique ou la virtualisation légère, qui
sont des domaines porteurs pour l'avenir. En effet, bien que plutôt
ancienne dans les environnements Solaris et BSD, l'utilisation des
conteneurs est quelque chose d'assez nouveau au sein du noyau Linux et
est en train de se généraliser depuis plusieurs mois.

\vspace{1em}

Par ailleurs, j'ai travaillé avec de nombreuses applications,
bibliothèques et technologies libres, et suis ravi d'avoir pu,
contribuer au nom de l'entreprise, au développement ou à l'amélioration
de plusieurs projets.

\vspace{1em}

Enfin, la diversité du travail effectué durant ce stage fut
particulièrement gratifiant.
