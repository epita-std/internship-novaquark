## Base de données

\url{http://influxdb.com/docs/} : *InfluxDB Documentation* : Documentation de la base de données InfluxDB.

## Déploiement

\url{http://docs.ansible.com/} : *Ansible Documentation* : Documentation de l'outil de déploiement Ansible.

## Virtualisation légère

\url{https://linuxcontainers.org/} : *LXC - Linux Containers* : Site de présentation des conteneurs Linux.

\url{https://docs.docker.com/} : *Docker Documentation* : Documentation du programme Docker.

\url{https://blog.flameeyes.eu/2010/09/linux-containers-and-networking}: *Linux Containers and Networking* : article présentant les différents types de réseau existants.

\url{https://www.stgraber.org/2013/12/20/lxc-1-0-blog-post-series/} : *LXC 1.0: Blog post series* : sommaire d'une série d'articles présentant LXC.

## Informatique en nuage

\url{https://aws.amazon.com/fr/} : *Amazon Web Services (AWS)* : point d'entrée vers la documentation des services d'informatique en nuage d'Amazon.

\url{https://cloud.google.com/products/compute-engine/} : *Google Compute Engine* : point d'entrée vers la documentation des services d'informatique en nuage de Google.
