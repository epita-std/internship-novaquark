%%define fimage($path , $ratio , $caption)
    return "\\begin{figure}[h]\n" .
    "\\begin{center}\n" .
    "\\includegraphics[width=$ratio\\textwidth]{$path}\n" .
    "\\caption{$caption}\n" .
	"\\label{fig:$path}" .
    "\\end{center}\n" .
    "\\end{figure}"
=end

# Résumé du stage
%%scoped-include(R_abstract.md)

# Introduction
%%scoped-include(R_intro.md)

# Aspects organisationnels
%%scoped-include(R_organisationnels.md)

# Aspects scientifiques et techniques
%%scoped-include(R_technique.md)

# Bilan
%%scoped-include(R_bilan.md)

# Glossaire
%%scoped-include(R_glossaire.md)

# Webographie
%%scoped-include(R_biblio.md)

\listoffigures
