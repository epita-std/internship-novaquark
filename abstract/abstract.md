%%define fimage($path , $ratio , $caption)
    return "\\begin{figure}[h]\n" .
    "\\begin{center}\n" .
    "\\includegraphics[width=$ratio\\textwidth]{$path}\n" .
    "\\caption{$caption}\n" .
	"\\label{fig:$path}" .
    "\\end{center}\n" .
    "\\end{figure}"
=end

## The Company: Novaquark

Novaquark is a video games studio created in January 2014 by Jean-Christophe
Baillie. He already launched the robotic company Gostai, bought by Aldebaran
in 2012.

\vspace{1em}

Novaquark team focuses itself on the development of a massively multiplayer
online game, called \Dual{}.

\vspace{1em}

%%fimage(../report/img/dual-dark.png, 0.4, "The game's logo")

\vspace{1em}

The studio lies in the startups' incubator Agoranov. When I started my
internship, four persons composed the team:

* Jean-Christophe Baillie: founder and CEO;
* Étienne Robin-Champigneul: COO;
* Jérome Jouvie: 3D developer;
* David Bernard: server developer.

\vspace{1em}

Since my arrival, the team has grown and now we count 10 persons including:

* Bertrand Remondin: lead artist;
* Mathias Labeyrie: 3D developer;
* Paul Chabas: client developer;
* Ludovic Serny: community manager;
* Étienne Baratte: server developer;
* and four other trainees.

\vspace{1em}

I started to work as *DevOps* in the server team with Mr Bernard. After his
departure, I continue to work with Mr Baillie and now with Mr Baratte.

## The Internship

My internship goal was to establish the basis of the servers' deployment
recipes needed to allow the automatic scalability of the game server
architecture. Then, when the server worked properly, I had to design the load
testing architecture.

\vspace{1em}

Numerous additional tasks were also assigned me throughout the internship;
especially to know my opinion about the implementation of some features that
can influence the deployment, load or security.

\vspace{1em}

Thanks to my recent experience in assistants laboratory and SRS laboratory as
system administrator and many personal experiences, I started comfortable with
virtualization technologies, basis of the cloud computing.

\vspace{1em}

Because of the company youth, there wasn't any work related to my internship
subject; others employees work on different problematics.

## Work

My internship can be split into two parts, but a timeline is not relevant
because I regularly handle multiple tasks in parallel depending on the needs of
the rest of the team.

\vspace{1em}

* **State of art:**
    * Cloud computing platforms analysis
    * Automatic deployment tools analysis
    * Virtualization technologies analysis
    * Ingame chat solution research
    * Authentication solution research

* **Developments:**
    * Design and implementation of a system metrics' gathering software
    * Writing some deployment recipes for servers
    * Creating a delivery process for virtual machines
    * Load testing the current server implementation
    * Implementation of game and business metrics' gathering into the game server
    * Looking for the best architecture for future servers deployment

## General appreciation

I worked both for the team (virtual machines, ...) and on long-term issues
(server, cloud scalability, ...). For each part, my work was received with
interest by those involved.

\vspace{1em}

This is an advantage of small business: we can communicate easily to design the
most appropriate solutions to requestor's demands. The project is still recent,
every week, we plan a meeting together to see the progress of each game
part. It's very motivating to see the project grow quickly.

\newpage

## Engineering skills acquired

Thanks to this internship, I acquired a lot of new skills like:

* operating system-level virtualization;
* automatic deployment;
* MMO game server;
* cloud computing scalability;
* free software distribution and participation in company context;
* metrics gathering;
* Go programming language.

## Conclusion

After my internship, Novaquark now has solid foundations for all aspects around
the game server: virtual machines for client developers, server deployment and
security through system containers and metrics gathering for future use in
scalability.

\vspace{1em}

On the other hand, I also clarified several issues remain obscure, such as
authentication, chat system or the architecture for deployment and update. This
will be set up when developers will have developed these points.

\vspace{1em}

I worked with many applications, libraries and open source technologies, and am
delighted to have been able to contribute in the name of the company. Finally,
the diversity of the work done during this internship was particularly
rewarding.
