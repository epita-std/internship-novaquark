#!/usr/bin/env perl

use 5.010_001;
use strict;
use warnings;
use utf8;
use open IO => 'utf8';
use open ':std';
use Data::Dumper;

use Getopt::Long;

use Utils::Say qw( process alert warning );

my $main = 'main.tex';
my $docname = '';
my $config_file = '';
GetOptions(
    'main=s' => \$main,
    'docname=s' => \$docname,
    'xml=s' => \$config_file,
    );

alert "no --docname given, can't build" unless $docname ne '';
alert "no --xml configuration file given, can't build" unless $config_file ne '';

use Utils::Config;
use Utils::Git;
use Make::Templates;

my ($config, $template, $final);
process {
    $config = Utils::Config->readXML($config_file);
} "Reading $config_file" => "Project configuration read";

process {
    $template = Make::Templates->new( $config->{project}, $docname );
} "Creating template";

process {
    $final = $template->assemble( $main );
} "Assembling template";

print $final;
